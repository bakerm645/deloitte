module "eks_cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "~> v5.0"

  cluster_name = "eks-cluster"
  vpc_id       = "${module.eks_vpc.vpc_id}"
  subnets      = "${module.eks_vpc.private_subnets}"

  worker_groups = [
    {
      instance_type = "m4.large"
      asg_max_size  = 5

      tags = [
        {
          key                 = "Name"
          value               = "eks-cluster"
          propagate_at_launch = true
        },
      ]
    },
  ]

  tags = {
    Name        = "eks-cluster"
    Environment = "dev"
  }
}
