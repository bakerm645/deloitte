terraform {
  backend "s3" {
    bucket = "concisemike-terraform-remote-state"
    key    = "technical-exercise.tfstate"
  }
}
