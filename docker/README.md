# Webserver Docker Container

## Running the Container:
1. Build the Docker image:
    ```
    docker build -t <image>:<tag> ./
    ```

1. Run the Docker image:
    ```
    docker run --rm -d -p 8080:8080 <image>:<tag>
    ```

## Design Decisions:
* The application was written in Golang due to familiarity with and preference to use the language, while also achieving satisfactory performance.
* The Dockerfile contains a multi-stage build to reduce the size of the final image:
  * The `build` stage uses the `golang:1.12-alpine` image: 350MB
  * The final stage uses the `alpine:latest` image: 5.58MB
  * The resulting image: 20.4MB
* The Dockerfile creates a system user and runs the command as this user to prevent granting the container root privileges.
